//soal nomer satu--------------------------------------------------------------------
//Looping Pertama
console.log('JAWABAN NOMOR 1:')

var flag = 2;
while(flag <= 20) {
  console.log(flag + ' - I Love you');
  flag+=2;
}

//Looping Kedua

var flag = 20;
while(flag >= 2) {
  console.log(flag + ' I will become a frontend developer');
  flag-=2;
}

//soal nomer dua---------------------------------------------------------------------
console.log('\nJAWABAN NOMOR 2:')

for(var angka = 1; angka <=20; angka++) {

	if (angka%2==1) {
		console.log(angka + ' - Santai');
	} else {
		console.log(angka + ' - I Love Coding');
	}

	if((angka%3==0) && (angka%2==1)) {
		console.log(angka + ' - Berkualitas');
	}

}

//soal nomer tiga-------------------------------------------------------------------
console.log('\nJAWABAN NOMOR 3:')


for (var j=1; j<=7; j++){

	for(var i=1; i<=j; i++){
		//ini buat print tanpa newline
		process.stdout.write('#');
	}
	console.log();

}

//soal nomer empat--------------------------------------------------------------------
console.log('\nJAWABAN NOMOR 4:')

var kalimat = "saya sangat senang belajar javascript";

var kata = kalimat.split(' ');
console.log(kata);

//soal nomer lima--------------------------------------------------------------------
console.log('\nJAWABAN NOMOR 5:')

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];


for (var index=0; index<5; index++){
	console.log(daftarBuah[index])
}